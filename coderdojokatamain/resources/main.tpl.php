
<!--Page 0 markup begins here-->

<div class="container-fluid">
	<div class="row dl-menu">
		<div class="col-xs-4 kata-home-panel organise">Organise</div>
		<div class="col-xs-4 kata-home-panel ninjas" style="">Ninjas</div>
		<div class="col-xs-4 kata-home-panel mentors">Mentors</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="logo kata home">
			<a href="https://coderdojo.com"><img alt="CoderDojo.org" src="<?php echo $viewHelper["ImagePath"], "logo.png"; ?>" width="48px" height="48px">
				<span class="kata-logo-text"><?php echo $viewHelper["SiteName"]; ?></span></a>
		</div>
		<span class="logo-text hidden-xs" style="">An open forum for the
			CoderDojo community to share resources with one another. </span>
	</div>
</div>


<div class="container-fluid">
	<div class="col-xs-4 kata kata-panel " style="">
		<div class="organise"></div>
		<img width="500"
			src="https://coderdojo.org/wp-content/uploads/2014/07/communitysupportmilano.png"
			alt="communitysupportmilano">
		<div style="" class="details">Detail text for Organise panel</div>
		<div class=" kata-home-panel kata-home-panel-button organise-button ">
			<a href="learnMore.html">Learn More</a>

		</div>
	</div>
	<div class="col-xs-4 kata kata-panel " style="">
		<div class="ninjas"></div>
		<img width="500"
			src="https://coderdojo.org/wp-content/uploads/2014/07/communitysupportmilano.png"
			alt="communitysupportmilano">
		<div class="details">Detail text for Organise panel</div>
		<div class=" kata-home-panel kata-home-panel-button explore-button ">
			<a href="learnMore.html">Explore</a>

		</div>
	</div>
	<div class="col-xs-4 kata kata-panel " style="">
		<div class="mentors"></div>
		<img width="500"
			src="https://coderdojo.org/wp-content/uploads/2014/07/communitysupportmilano.png"
			alt="communitysupportmilano">
		<div class="details">Detail text for Organise panel</div>
		<div
			class=" kata-home-panel kata-home-panel-button view-tutorials-button">
			<a href="learnMore.html">View Tutorials</a>

		</div>
	</div>


</div>
<!--Page 0 markup ends here-->
